class CreateTestdata < ActiveRecord::Migration
  def change
    create_table :testdata do |t|
      t.date:date
      t.time:time
      t.decimal:var1
      t.decimal:var2
      t.decimal:var3
      t.decimal:var4
      t.decimal:var5
      t.decimal:var6
      t.decimal:var7
      t.decimal:var8

      t.timestamps null: false
    end
  end
end
