class Testdatum < ActiveRecord::Base
    require 'csv'
    def self.import(file)
        CSV.foreach(file.path, headers: true) do |row|
            Testdatum.create! row.to_hash
        end
    end
    
    # def self.import(file)
    #     CSV.foreach(file.path, headers:true, header_converters: symbol) do |row|
    #         Stuff.create! row.to_hash
    #     end
    # end
    
#   include Mongoid::Document
#   include Mongoid::Timestamps

#   def self.import!(file)
#     columns = []
#     instances = []
#     CSV.foreach(file.path) do |row|
#       if columns.empty?
#         # We dont want attributes with whitespaces
#         columns = row.collect { |c| c.downcase.gsub(' ', '_') }
#         next
#       end

#       instances << create!(build_attributes(row, columns))
#     end
#     instances
#   end

#   private

#   def self.build_attributes(row, columns)
#     attrs = {}
#     columns.each_with_index do |column, index|
#       attrs[column] = row[index]
#     end
#     attrs
#   end

end
