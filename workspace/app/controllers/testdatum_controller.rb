class TestdatumController < ApplicationController
  def index
    @testdatum = Testdatum.all
  end

  def import
    Testdatum.import(params[:file])
    redirect_to root_url, notice: "Activity Data imported!" 
  end
end
